import React from 'react';
import './App.css';
import "bootstrap/dist/css/bootstrap.min.css";
import {BrowserRouter} from "react-router-dom";
import NavBar from "./components/nav";
import RouterSwitch from "./components/router";

function App() {
  return (
      <BrowserRouter>

        <NavBar />
        <RouterSwitch />

        </BrowserRouter>
  );
}

export default App;
