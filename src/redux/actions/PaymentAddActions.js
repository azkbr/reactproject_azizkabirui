import {PAYMENT_ADD_SUCCESS , PAYMENT_ADD_FAIL} from "../slices/PaymentAddSlice"

import PaymentRestServices from "../../services/PaymentRestServices";

//Use Async await rather than then
const paymentAdd = (payment) => async (dispatch) => {
    try {
        PaymentRestServices.postNewPayment(payment).then(res => dispatch(PAYMENT_ADD_SUCCESS(res.status)));
    } catch (error) {
        dispatch(PAYMENT_ADD_FAIL(error)); //Or Error
    }
};


export  default  paymentAdd;