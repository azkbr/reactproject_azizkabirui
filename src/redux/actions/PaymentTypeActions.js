import {PAYMENT_LIST_FAIL, PAYMENT_LIST_SUCCESS} from "../slices/PaymentListSlice"
import PaymentRestService from "../../services/PaymentRestServices";


const retrievePaymentList = (paymentType) => (dispatch) => {
    try {
            PaymentRestService.getPaymentByType(paymentType.type).then(        //axios.get(CUSTOMERS_URI);
                response => {
                    dispatch(PAYMENT_LIST_SUCCESS(response.data));
                });
    } catch (error) {
        console.log(">>> in dispatch error=" + error);
        dispatch(PAYMENT_LIST_FAIL(error));
    }
};
export default retrievePaymentList;