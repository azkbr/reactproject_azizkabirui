import { createSlice } from '@reduxjs/toolkit';

export const paymentAddSlice = createSlice({
  name: 'PaymentAddReducer',
  initialState: {
    payment: {},
    error: '',
    submitted: false,
  },
  reducers: {
    PAYMENT_ADD_SUCCESS: (state, action) => {
      state.submitted = true;
      state.payment = action.payload;
    },
    PAYMENT_ADD_FAIL: (state, action) => {
      state.submitted = true;
      state.error = action.payload;
    },
  },
});

export const {

  PAYMENT_ADD_SUCCESS,
  PAYMENT_ADD_FAIL,
} = paymentAddSlice.actions;

export default paymentAddSlice.reducer;
