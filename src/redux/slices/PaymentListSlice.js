import { createSlice } from '@reduxjs/toolkit';

export const paymentListSlice = createSlice({
  name: 'PaymentListReducer',
  initialState: {
    pending: false,
    payments: [],
    error: null,
  },
  reducers: {
    PAYMENT_LIST_SUCCESS: (state, action) => {
      state.pending = false;
      state.payments = action.payload;
    },
    PAYMENT_LIST_FAIL: (state, action) => {
      state.pending = false;
      state.error = action.payload;
    },
  },
});

export const {
    PAYMENT_LIST_SUCCESS,
  PAYMENT_LIST_FAIL,
} = paymentListSlice.actions;


export default paymentListSlice.reducer;
