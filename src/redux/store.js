import { configureStore } from '@reduxjs/toolkit';
import paymentAddSlice from './slices/PaymentAddSlice';
import paymentListSlice from './slices/PaymentListSlice';

export default configureStore({
 reducer: {
  PaymentAddReducer: paymentAddSlice,
  PaymentListReducer: paymentListSlice,
 },
});
