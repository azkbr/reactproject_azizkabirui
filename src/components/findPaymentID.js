import React, {useState} from "react";
import PaymentDetailForm from "./paymentDetailForm";
import PaymentRestServices from "../services/PaymentRestServices";

function PaymentID() {
     const initialFormState = {
         id: 0,
         submitted: false
     };
     const [searchType, setSearchType] = useState(initialFormState);
     const handleInputChange = (event) => {
         const {name, value} = event.target;
         setSearchType({...searchType, [name]: value});
     };

     const [payment, setPayment] = useState({
         id: 0,
         paymentDate: "",
         type: "",
         custId: 0,
         amount: 0
     });
     const handleAPIResponse = (response) => {
         setPayment({
             payment,
             id: response.data.id,
             paymentDate: response.data.paymentDate,
             custId: response.data.custId,
             type: response.data.type,
             amount: response.data.amount
         });
     };


     async function getData(searchType) {
         await PaymentRestServices.getPaymentById(searchType.id)
             .then(response => handleAPIResponse(response))
             .catch(e => {
                 console.log(e);
             });
     }

    return (
        <div className="col-md-6">
            <h4>Find by Payment ID</h4>
            <p>Search for a payment by the Payment ID</p>
            <form className="form-inline"
                  onSubmit={(event) => {
                      event.preventDefault();
                      getData(searchType);
                  }}
            >
                <div className="form-group mx-sm-3 mb-2">
                    <label htmlFor="type" className="sr-only">Find By ID</label>
                    <input required type="number" className="form-control" id="id" name="id" placeholder="Find by ID"
                           value={searchType.id} onChange={handleInputChange}/>
                </div>
                <button type="submit" className="btn btn-primary mb-2">Search</button>
            </form>
            {payment.id > 0 ?
                <PaymentDetailForm payment={payment}/>
                :
                <div/>}


        </div>


    )
}

export default PaymentID;