import React from "react";
import {Link} from "react-router-dom";

export default function NavBar() {
    return (
        <nav className="navbar navbar-expand navbar-dark bg-dark">
            <a href="/" className="navbar-brand">
                Payment System
            </a>
            <div className="navbar-nav mr-auto">
                <li className="nav-item">
                    <Link to={"/newPayment"} className="nav-link">
                        New Payment
                    </Link>
                </li>
                <li className="nav-item">
                    <Link to={"/findPaymentTypes"} className="nav-link">
                        Find by Payment Type
                    </Link>
                </li>
                <li className="nav-item">
                    <Link to={"/findPaymentID"} className="nav-link">
                        Find by Payment ID
                    </Link>
                </li>
            </div>
        </nav>
    );
}