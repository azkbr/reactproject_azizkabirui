import React from "react";
import PaymentDetailForm from "./paymentDetailForm";

export default function PaymentDetail(props) {

    return (
        <div className="col-md-6">
            <h4>Payment Details</h4>
            <p>Detailed view of Payments</p>
            {props ?
                <PaymentDetailForm payment={props}/>
                :
                <div/>}
        </div>


    )
}