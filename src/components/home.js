import React,{ Component }  from 'react';
import Jumbotron from 'react-bootstrap/Jumbotron'
import Container from "react-bootstrap/Container";
import PaymentRestServices from "../services/PaymentRestServices";

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = { status: "185", rowCount: 0 };
    }

    async  componentDidMount(){
        try
        {
            PaymentRestServices.getStatus().then(result=> this.setState({ status: result.data})).catch(e => {console.log(e);});
            PaymentRestServices.getPaymentRowCount().then(result=> this.setState({ rowCount: result.data})).catch(e => {console.log(e);});
        }
        catch(e)
        {
            console.log('The err is: ' + e.toString())
        }
        return this.state;
    }

    render(){
        return (
            <Jumbotron>
                <Container>
                    <h1>Payment System</h1>
                    <p>API Status: {this.state.status}</p>
                     <p>Number of records in the database: {this.state.rowCount}</p>
                </Container>
            </Jumbotron>

        )
    }

}
export default Home;
