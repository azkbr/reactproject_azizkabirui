import React from "react";
import {Route, Switch} from "react-router-dom";
import newPayment from "./newPayment";
import Home from "./home";
import PaymentID from "./findPaymentID";
import ListPaymentType from "./findPaymentTypes";
import PaymentDetail from "./paymentDetail";

export default function RouterSwitch() {
    return (
        <div className="container mt-3">
            <Switch>
                <Route exact path={["/", "/home"]} component={Home}/>
                <Route exact path={["/", "/newPayment"]} component={newPayment}/>
                <Route exact path={["/", "/findPaymentTypes"]} component={ListPaymentType}/>
                <Route exact path={["/", "/findPaymentID"]} component={PaymentID}/>
                <Route exact path={["/", "/paymentDetails"]} component={PaymentDetail}/>
            </Switch>
        </div>
    );
}
