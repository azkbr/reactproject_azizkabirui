import React from "react";
import PaymentDetailForm from "./paymentDetailForm";

function NewPayment() {
    const initialFormState = {
        id: 0,
        paymentDate: "",
        type: "",
        custId: 0,
        amount: 0,
        submitted: false
    };

    return (
        <div>
            <h4>Add new Payment</h4>
            <PaymentDetailForm payment={initialFormState}/>
        </div>
    );
}
export default NewPayment;
