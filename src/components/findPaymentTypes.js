import React, {useState} from "react";
import {useHistory} from 'react-router-dom';
import {useDispatch, useSelector} from "react-redux";
import retrievePaymentList from "../redux/actions/PaymentTypeActions";


function ListPaymentType() {
    const dispatch = useDispatch();
    const paymentList = useSelector(state => state.PaymentListReducer.payments);

    const initialFormState = {
        type: '',
        submitted: false
    };
    const [searchType, setSearchType] = useState(initialFormState);
    const handleInputChange = (event) => {
        const {name, value} = event.target;
        setSearchType({...searchType, [name]: value});
    };

    const history = useHistory();
    const handleClickThrough = (event) => {
        history.push('/paymentDetails', event)
    }

    return (

        <div className="col-md-6">
            <h4>Find by Payment Type</h4>
            <p>Search for a payment by the Payment Type</p>
            <form className="form-inline"
                  onSubmit={(event) => {
                      event.preventDefault();
                      dispatch(retrievePaymentList(searchType));

                  }}
            >
                <div className="form-group mx-sm-3 mb-2">
                    <label htmlFor="type" className="sr-only">Password</label>
                    <select type="text" className="form-control" id="type" name="type" placeholder="Find by Type"
                           value={searchType.type} onChange={handleInputChange}>
                        <option defaultValue>Choose...</option>
                        <option value="MASTERCARD">MASTERCARD</option>
                        <option value="VISA">VISA</option>
                        <option value="BACS">BACS</option>
                        <option value="CHEQUE">CHEQUE</option>
                        <option value="CASH">CASH</option>
                        <option value="PAYPAL">PAYPAL</option>
                        <option value="BITCOIN">BITCOIN</option>
                    </select>
                </div>
                <button type="submit" className="btn btn-primary mb-2">Search</button>
            </form>
            <div className="list-group">

                <table className="table  table-hover">
                    <tbody>
                    {paymentList && paymentList.map((payment, index) => {
                        return [
                            <tr key={index} onClick={() => {
                                handleClickThrough(payment)
                            }}>
                                <th scope="row">{payment.id}</th>
                                <td>{payment.type}</td>
                                <td>{payment.custId}</td>
                                <td>{payment.amount}</td>
                                <td>{payment.paymentDate}</td>
                            </tr>
                        ];
                    })}
                    </tbody>
                </table>
            </div>
        </div>
    )

}

export default ListPaymentType;