import React, {useState} from "react";
import {useDispatch} from "react-redux";
import paymentAdd from "../redux/actions/PaymentAddActions";
import {useHistory} from "react-router-dom";

export default function PaymentDetailForm(props) {
    const dispatch = useDispatch();
    let findState

    if (props.payment.id && props.payment.id > 0) {
        findState = props.payment
    } else if (props.payment.history) {
        findState = props.payment.history.location.state
    } else {
        findState = {
            id: 0,
            paymentDate: new Date().toISOString(),
            type: "",
            custId: 0,
            amount: 0
        }
    }

    const initialFormState = findState;
    const [payment, setPayment] = useState(initialFormState);
    const handleInputChange = (event) => {
        const {name, value} = event.target;
        setPayment({...payment, [name]: value});
    };

    const history = useHistory();
    const handleClickThrough = () => {
        history.push('/home')
    }

    function validateForm(payment) {
        if(!payment.id>0 || payment.id.trim()===''){
            alert('Payment Id can not be empty!')
        } else if(!payment.custId>0 || payment.custId.trim()===''){
            alert('Customer Id can not be empty!')
        } else if(!payment.amount>0|| Number.isNaN(parseInt(payment.amount)) || payment.custId.trim()===''){
            alert('Enter Valid Amount!')
        } else if(!payment.paymentDate instanceof Date && !isNaN(payment.paymentDate)){
            alert('Enter Valid Date!')
        } else {
            return true;
        }
    }

    return (
        <div>
            <form className="needs-validation" noValidate
                  onSubmit={(event) => {
                    event.preventDefault();
                    if(validateForm(payment)){
                        dispatch(paymentAdd(payment));
                        handleClickThrough()
                    }
                }}>
                <div className="form-row">
                    <div className="col-md-4 mb-3">
                        <label htmlFor="id">Payment Id</label>
                        <input type="number" className="form-control" id="id" name="id"
                               placeholder="Payment Id" value={payment.id} required
                               onChange={handleInputChange}
                        />
                    </div>

                    <div className="col-md-4 mb-3">
                        <label htmlFor="type">Choose Payment Type</label>
                        <select  type="text" className="form-control" id="type" name="type"
                               placeholder="Enter Payment Type" value={payment.type} required
                               onChange={handleInputChange}>
                            <option value="MASTERCARD">MASTERCARD</option>
                            <option value="VISA">VISA</option>
                            <option value="BACS">BACS</option>
                            <option value="CHEQUE">CHEQUE</option>
                            <option value="CASH">CASH</option>
                            <option value="PAYPAL">PAYPAL</option>
                            <option value="BITCOIN">BITCOIN</option>
                        </select>
                    </div>
                    <div className="col-md-4 mb-3">
                        <label htmlFor="custId">Customer Id</label>
                        <input type="number" className="form-control" id="custId" name="custId"
                               placeholder="Enter Customer ID" value={payment.custId} required
                               onChange={handleInputChange}
                        />
                    </div>
                </div>
                <div className="form-row">
                    <div className="col-md-6 mb-3">
                        <label htmlFor="amount">Payment Amount</label>
                        <input type="number" className="form-control" id="amount" name="amount"
                               placeholder="Enter amount" value={payment.amount} required
                               onChange={handleInputChange}
                        />
                    </div>
                    <div className="col-md-6 mb-3">
                        <label htmlFor="paymentDate">Payment Date</label>
                        <input type="date" className="form-control" id="paymentDate" name="paymentDate"
                               placeholder="Enter date" value={payment.paymentDate}
                               onChange={handleInputChange}
                        />
                    </div>
                </div>
                <div className="form-group">
                    <input type="submit" className="btn btn-primary" value="Save Payment"/>
                </div>
            </form>
        </div>

    )
}