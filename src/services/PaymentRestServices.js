import http from "../http-common";

export class PaymentRestServices {
    getPaymentRowCount(){
        return http.get('/rowCount');
    }

    getStatus(){
        return http.get('/status');
    }

    getPaymentById(id){
        return http.get(`/findById/${id}`);
    }

    getPaymentByType(type){
        return http.get('/findByType', {params: { type: type }});
    }

    postNewPayment(object){
        return http.post('/save', object);
    }

}

export default new PaymentRestServices();